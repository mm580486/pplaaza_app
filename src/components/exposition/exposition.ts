import { Component, Input  } from '@angular/core';
import { AgornaApi } from '../../shared/shared';
import { ShopDetailPage } from '../../pages/pages';
import { NavController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
export class Exposition {
  id: number;
  name: string;
  price: string;
  off_price: string;
  has_off_price: boolean;
  poster: string;
  followed:boolean;
}



@Component({
  selector: 'exposition',
  templateUrl: 'exposition.html'
}) 

export class ExpositionComponent {
  @Input() exposition: Exposition;
  user: any;
  constructor(public navCtrl: NavController,public storage: Storage,private agornaApi: AgornaApi) {
    this.storage.get('user').then((value) => {
      this.user=value;
    });
  }


  follow(pos){
          this.agornaApi.getFollow(this.exposition.id,this.user.authentication_token,pos).then(data => {
    
            console.log(data)
    
    this.exposition.followed = !pos;
    
          }, (err) => {
 
     
          });
    
    
    
    }


    goToShopPage(){
      this.navCtrl.push(ShopDetailPage,this.exposition)
    }
}

