import { Component, Input  } from '@angular/core';
import { NavController } from 'ionic-angular';

export class Product {
  id: number;
  name: string;
  price: string;
  off_price: string;
  has_off_price: boolean;
  poster: string;
}



@Component({
  selector: 'product',
  templateUrl: 'product.html'
}) 

export class ProductComponent {
  @Input() product: Product;
  constructor() {
 
  }



 

}

