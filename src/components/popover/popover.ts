import { Component } from '@angular/core';
import {  NavController,ToastController,LoadingController,NavParams ,ViewController } from 'ionic-angular';
/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
import { AgornaApi } from '../../shared/shared';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class PopoverComponent {
loading: any;
callback: any;
type: string;
token: string;
  constructor(private params: NavParams,public storage: Storage, private viewCtrl: ViewController,public navCtrl: NavController,private agornaApi: AgornaApi, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
  this.callback = params.get('cb');
  this.type = params.get('type');
  
  this.storage.get('token').then((value) => {
    this.token=value;
  });

}



  public loadc(x) {
   this.callback(x);

  this.viewCtrl.dismiss();
  // Close the popover

}

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }

}
