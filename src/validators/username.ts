
import { FormControl } from '@angular/forms';
export class UsernameValidator {

static isValid(control: FormControl): any {
 
        if(isNaN(control.value)){
            return {
                "not a username": true
            };
        }
 
        if(control.value.length < 3){
            return {
                "too short": true
            };
        }
 
        return null;
    }

    
  }
 
