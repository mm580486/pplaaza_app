import { FormControl } from '@angular/forms';
 
export class MobileValidator {
 
    static isValid(control: FormControl): any {
 
        if(isNaN(control.value)){
            return {
                "not a number": true
            };
        }
 
         if(/^(\+98|0)?9\d{9}$/.test(control.value) == false){
            return {
                "not a valid mobile number": true
            };
        }

      if(/^(\+98|0)?9(1+|2+|3+|4+|5+|6+|7+|8+|9+|0+)?$/.test(control.value) == true){
            return {
                "not a valid mobile number": true
            };
        }

 
        if(control.value.length > 15){
            return {
                "too long": true
            };
        }
 
      
 
        return null;
    }
 
}