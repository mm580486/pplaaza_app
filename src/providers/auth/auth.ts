import { Injectable } from '@angular/core';
import { Http, Headers,RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Injectable()
export class Auth {
 
  public token: any;
  private baseUrl = 'http://pplaaza.ir/api/v1';
  
  constructor(public http: Http, public storage: Storage) {
 
  }
 
  checkAuthentication(){
 
    return new Promise((resolve, reject) => {
 
        //Load token if exists
        this.storage.get('token').then((value) => {
 
            this.token = value;
 
            let headers = new Headers();
            headers.append('Authorization', this.token);
 
            this.http.get(`${this.baseUrl}/checkAuthentication.json`, {headers: headers})
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                }); 
 
        });         
 
    });
 
  }
 
  createAccount(details){
 
    return new Promise((resolve, reject) => {
 
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
 console.log(JSON.stringify(details));

  let body = new FormData();
  body.append('formdata',JSON.stringify(details));
        this.http.post(`${this.baseUrl}/register.json`, body, headers)
          .subscribe(res => {
  
            let data = res.json();
            this.token = data.authentication_token;
            this.storage.set('token', data.authentication_token);
             this.storage.set('user', data);
            resolve(data);
 
          }, (err) => {
            reject(err);
          });
 
    });
 
  }
 
  login(credentials){
 
    return new Promise((resolve, reject) => {
 
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = new FormData();
        body.append('formdata',JSON.stringify(credentials));

        this.http.post(`${this.baseUrl}/login.json`, body,headers)
          .subscribe(res => {
 
            let data = res.json();
            this.token = data.authentication_token;
            this.storage.set('token', data.authentication_token);
            this.storage.set('user', data);
            var storage = window.localStorage;
            storage.setItem('token', data.authentication_token);

            resolve(data);
 
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
 
    });
 
  }
 
  logout(){
    this.storage.set('token', '');
    this.storage.set('user', '');
  }
 
}