import { Component,ViewChild,ElementRef } from '@angular/core';
import { NavController,ToastController,LoadingController,NavParams,ViewController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { ShopDetailPage } from '../pages'
import { Geolocation } from '@ionic-native/geolocation';
declare var google;
@Component({
  selector: 'page-nearby',
  templateUrl: 'nearby.html'
})

export class NearbyPage {
  nearby: String='map';
  @ViewChild('map') mapElement: ElementRef;
  loading: any;
  map: any;
  expositions:any;
  expositions_empty:boolean=false;
  latitude: number;
  longitude: number;
  constructor(private geolocation: Geolocation,public viewCtrl: ViewController,private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {

  }
  ionViewDidEnter(){

// this.loadMap(); 
      this.geolocation.getCurrentPosition().then((resp) => {
      this.showLoader();
      this.agornaApi.getNearby(resp.coords.latitude,resp.coords.longitude).then(data => {
        
        this.expositions = data;
        if(this.expositions.length == 0){
        this.expositions_empty=true;
        }else{
        this.expositions_empty=false;
        }

    let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      maxZoom: 15,
      minZoom: 9,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);


  let marker = new google.maps.Marker({
    icon: {url: '/assets/images/map-marker.png'},
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: this.map.getCenter()
  });

  let content = "<h5>موقعیت شما</h5>";          

  this.addInfoWindow(marker, content);

  google.maps.event.addListener(this.map,'dragend',event => {
    var p1 = new google.maps.LatLng( event.latLng.lat(), event.latLng.lng());
    var p2 = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
    // if (((google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2)) > 50) {
    //   // Do something
    //   alert((google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2));
      
    // }
    //calculates distance between two points in km's
    var releaseLoc = p1 + ', ' + p2;
    alert(releaseLoc);
  });
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
          this.loading.dismiss();
        });
        }).catch((error) => {
          console.log('Error getting location', error);
          
          this.toastCtrl.create({
                  message: 'لطفا لوکیشن خود را روشن کنید',
                  duration: 3000,
                  position: 'bottom'
                }).present();
        });
  }

    //  calcDistance(p1, p2){
    //   return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
    // }

    showLoader(){
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
        this.loading.present();
    }

    goToExposition($event,item){
      this.navCtrl.push(ShopDetailPage,item)
    }

    loadMap(){

    }


   addInfoWindow(marker, content){

      let infoWindow = new google.maps.InfoWindow({
        content: content
      });

      google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(this.map, marker);
      });

    }

}


