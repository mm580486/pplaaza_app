import { Component, Input, ViewChild } from '@angular/core';
import { AgornaApi } from '../../shared/shared';
import { NavController,ToastController,LoadingController,NavParams ,ViewController } from 'ionic-angular';
import { ShopDetailPage,ProductDetailPage } from '../pages';
import { Storage } from '@ionic/storage';
import $ from 'jquery';
declare var window;
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
 q: string;
 filter: any='product';
 loading: any;
 products:any=[];
 products_empty:boolean=false;
 expositions_empty:boolean=false;
 expositions:any=[];
 user:any;
 @ViewChild('input') myInput ;
  constructor(public storage: Storage,public viewCtrl: ViewController,public navCtrl: NavController,private navParams: NavParams,private agornaApi: AgornaApi, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
  
  this.storage.get('user').then((value) => {
    this.user=value;
  });


  


  }



  ionViewDidEnter(){

    document.getElementsByClassName('searchbar-search-icon')[0].addEventListener('click',()=> this.search());
  
    setTimeout(() => {
      this.myInput.setFocus();
    },150);
  }
  goToShop(item){
    this.navCtrl.push(ShopDetailPage,item);
  }
  
  goToProduct(item){
    this.navCtrl.push(ProductDetailPage,item);
  }


toggleFollow($event,item,pos){


      this.agornaApi.getFollow(item.id,this.user.authentication_token,pos).then(data => {

        console.log(data);

item.followed = !pos;

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
      });


}



  search(){
    
    if(this.q == '' || this.q == 'undefined'){

      return false
    }
      this.showLoader();


      this.agornaApi.search(this.filter,this.q,this.user.authentication_token).then(data => {
        if(this.filter=='product'){
 this.products = data;
if(this.products['products'].length == 0){
  this.products_empty=true;
}else{
  this.products_empty=false;
}

        }else{
          this.expositions=data;

          if(this.expositions['expositions'].length == 0){
  this.expositions_empty=true;
}else{
  this.expositions_empty=false;
}


        }
        console.log(data);

      }, (err) => {

 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
        });


        this.loading.dismiss();

    
  }


  

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }
goToExposition($event,item){
this.navCtrl.push(ShopDetailPage,item);
}


}
