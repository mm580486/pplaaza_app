import { Component,ElementRef, Directive, ViewChild } from '@angular/core';
import {  PopoverController,NavController,ToastController,LoadingController,NavParams,ViewController  } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { Storage } from '@ionic/storage';
import { ShopDetailPage,ProductDetailPage } from '../pages'
import { Content } from 'ionic-angular';
import { PopoverComponent } from '../../components/popover/popover';
import * as $ from 'jquery';

@Component({
  selector: 'page-conversation',
  templateUrl: 'conversation.html'
})


export class ConversationPage {

loading: any;
messages: any=[];
token: any;
hideTime: boolean;
message: string;
products: any;
productsVisible: boolean=false;
public user: any;
arr: any;
interval: any;
opponent_name: any;
opponent_id: any;
@ViewChild(Content) content: Content;
  constructor(public popoverCtrl: PopoverController,public element:ElementRef,public viewCtrl: ViewController,  public storage: Storage,private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
    this.element = element;

    this.storage.get('user').then((value) => {
    this.user=value;
  });

  this.interval=setInterval(() => { this.checkNewMessage() }, 5000);





  }


  toggleProducts(){
    this.productsVisible = !this.productsVisible;

  }



  presentPopover(myEvent,item) {
    
    
    
        let popover = this.popoverCtrl.create(PopoverComponent,{
      cb: function(_data) {
alert(_data);
  
          }
     , type: 'self'
        
    
        });
        popover.present({
          ev: myEvent
        });
    
      }



 checkNewMessage(){

this.agornaApi.checkNewMessage(this.user.authentication_token,this.navParams.data.id,this.messages[this.messages.length-1].id).then(data => {
        this.arr= data;
        if(this.arr.length > 0){

          for (var i = 0; i < this.arr.length; i++) {

  this.messages.push({
   id: this.arr[i].id,
   user_id: this.arr[i].user_id,
   message: this.arr[i].message,
   time: this.arr[i].time
   })

          }


        }

       
        
        console.log(this.arr)
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();




        });



  let classname = document.getElementsByClassName("product-image");

for (var i = 0; i < classname.length; i++) {
    // classname[i].addEventListener('click', myFunction, false);
    classname[i].addEventListener(`click`, (evt) => this.gtp(evt));
}





}

sendProductToChat(item){

  this.agornaApi.saveConversation(this.user.authentication_token,this.navParams.data.id,`$${item.id} ${item.name}`).then(data => {
    console.log(data);
    this.messages.push(data);
    this.message='';
     this.loading.dismiss();
     this.content.resize();
     this.content.scrollToBottom(100);
     this.content.scrollTo(0, 11500, 200);
  }, (err) => {
this.toastCtrl.create({
      message: 'اشکال در برقراری ارتباط با سرور',
      duration: 3000,
      position: 'bottom'
    }).present();
    });


}
gtp(event){

  var target = event.target || event.srcElement || event.currentTarget;
  var classname=target.attributes.class.nodeValue;
  var product_id= classname.match(/\b\d*/);
  this.navCtrl.push(ProductDetailPage,{id: product_id})
}
toggled: boolean = false;
emojitext: string;
 
handleSelection(event) {
  this.message = this.message + " " + event.char;
}

changeSize(){
  this.element.nativeElement.querySelector("textarea").style.height =this.element.nativeElement.querySelector("textarea").scrollHeight;
}
  ionViewDidEnter(){
this.viewCtrl.showBackButton(false);

this.agornaApi.getProducts('last_products').then(data => {
  this.products = data;


  }, (err) => {
this.toastCtrl.create({
      message: 'اشکال در برقراری ارتباط با سرور',
      duration: 3000,
      position: 'bottom'
    }).present();


  });


this.storage.get('user').then((value) => {
  this.user=value;


      this.showLoader();
      console.log(this.navParams.data);


if(this.navParams.data.message != ''){
      this.agornaApi.saveConversation(this.user.authentication_token,this.navParams.data.id,this.navParams.data.default_message).then(data => {
        this.messages.push(data);
      }, (err) => {
        });
      }




      this.agornaApi.getConversation(this.user.authentication_token,this.navParams.data.id,this.navParams.data.message).then(data => {
        this.messages = data['conversations'];
this.opponent_name = data['opponent_name'];
this.opponent_id = data['opponent_id'];
        console.log(data)
         this.loading.dismiss();
         this.content.resize();
         this.content.scrollToBottom(100);
         this.content.scrollTo(0, 11500, 200);
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });

    

      });

      this.content.resize();
      this.content.scrollToBottom(100);
      this.content.scrollTo(0, 11500, 200);
  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }

    ionViewDidLeave() {
      clearInterval(this.interval);
    }

    sendMessage(){
      this.agornaApi.saveConversation(this.user.authentication_token,this.navParams.data.id,this.message).then(data => {
        console.log(data);
        this.messages.push(data);
        this.message='';
         this.loading.dismiss();
         this.content.resize();
         this.content.scrollToBottom(100);
         this.content.scrollTo(0, 11500, 200);
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
        });




    }


goToShop(){
  if(this.user.level == 0){
    this.navCtrl.push(ShopDetailPage,{id: this.opponent_id})
  }
    
}

goToProduct(id){
  this.navCtrl.push(ProductDetailPage,{id: id});
}


}


