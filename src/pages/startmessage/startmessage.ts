import { Component } from '@angular/core';
import { NavController,ToastController,ViewController,LoadingController,NavParams  } from 'ionic-angular';
import { AgornaApi } from '../../shared/shared';
import { Storage } from '@ionic/storage';
import { ConversationPage } from '../pages';
@Component({
  selector: 'page-startmessage',
  templateUrl: 'startmessage.html'
})
export class StartmessagePage {
  title:string='';
  body:string='';
  user:any;
  exposition:any;
  nothin:any=[];
  constructor(public viewCtrl: ViewController,private navParams: NavParams, public storage: Storage,public navCtrl: NavController,private agornaApi: AgornaApi, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
this.exposition=this.navParams.data;

  }

    ionViewDidEnter(){
    this.viewCtrl.showBackButton(false);
      this.storage.get('user').then((value) => {
    this.user=value;
  });





    }

    sendMessage(){




      this.agornaApi.getStartMessage(this.user.authentication_token,this.exposition.id,this.title,this.body).then(data => {



        this.navCtrl.push(ConversationPage,data);



      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
      });







    }

}
