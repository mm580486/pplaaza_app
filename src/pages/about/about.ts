import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
declare var window;
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController) {

  }
callTo(number){
  number=encodeURIComponent(number);
  window.location = 'tel:'+number;
}
mailTo(mail){
  window.location = 'mailto:'+mail;
}

}
