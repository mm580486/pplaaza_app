import { Component } from '@angular/core';
import { Platform,ActionSheetController,NavController,ToastController,LoadingController,NavParams,ViewController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { SubcategoriesPage } from '../pages';
import { Storage } from '@ionic/storage';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import $ from 'jquery';
import { SelfProductPage } from '../pages';
import {DomSanitizer} from '@angular/platform-browser';



declare var cordova: any;
@Component({
  selector: 'page-buildProduct',
  templateUrl: 'buildProduct.html'
})





export class BuildProductPage {

  categories: any;
  loading: any;
  user: any;
  product:any={};
  fields: any;
  base64: string;
  lastImage: any = [];
  constructor(private camera: Camera,public DomSanitizer: DomSanitizer,
   private transfer: Transfer,
    private file: File, 
    private filePath: FilePath,
     public actionSheetCtrl: ActionSheetController,
     public viewCtrl: ViewController,
     public storage: Storage,
     public navCtrl: NavController
     ,private navParams: NavParams,
     private agornaApi: AgornaApi,
        public toastCtrl: ToastController,
         public platform: Platform, 
         public loadingCtrl: LoadingController) {
    
    this.storage.get('user').then((value) => {
    this.user=value;
  });





}
  ionViewDidLoad(){



setTimeout(() => {
  $(document).ready(function(){

  })

                  this.showLoader();
      this.agornaApi.getProductCategories(this.user.authentication_token).then(data => {
        this.categories = data;
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
        });

    }, 1000);



  }


  categorySelect($event,item){
      this.showLoader();
      this.agornaApi.getCategoryFields(this.user.authentication_token,item.id).then(data => {
        this.fields = data;
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
        });




  }


  ionViewDidEnter(){
this.viewCtrl.showBackButton(false);
    // this.agornaApi.getCategories().then(data => this.categories = data);



  }




     showLoader(){
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
        this.loading.present();
    }




      logForm() {
console.log(this.product);
this.product.images=this.lastImage.join('@');
                  this.showLoader();

      this.agornaApi.saveProduct(this.user.authentication_token,this.product).then(data => {
         this.loading.dismiss();
          this.toastCtrl.create({
          message: 'محصول ثبت شد',
          duration: 3000,
          position: 'bottom'
        }).present();



setTimeout(() => {
this.navCtrl.push(SelfProductPage);

},1000);


console.log(data)

      }, (err) => {

        console.log(err);

 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
        });


//       this.showLoader();
//       this.agornaApi.updateProfile(this.user.authentication_token,this.form_user).then(data => {
// this.storage.set('user', data);
// this.form_user=data;
// console.log(data)
//  this.toastCtrl.create({
//           message: 'پروفایل بروز شد',
//           duration: 3000,
//           position: 'bottom'
//         }).present();

//          this.loading.dismiss();
//       }, (err) => {
//  this.toastCtrl.create({
//           message: 'اشکال در برقراری ارتباط با سرور',
//           duration: 3000,
//           position: 'bottom'
//         }).present();



//  this.loading.dismiss();
        // });



  }






 public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'شیوه ارسال عکس',
      buttons: [
        {
          text: 'گالری',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.SAVEDPHOTOALBUM);
          }
        },
        {
          text: 'گرفتن عکس',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'انصراف',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }


  public takePicture(sourceType) {
  // Create options for the Camera Dialog
  var options = {
    quality: 100,
    sourceType: sourceType,
    encodingType: this.camera.EncodingType.JPEG,
    allowEdit: true,
    saveToPhotoAlbum: true,
    targetWidth: 600,
    targetHeight: 600,
    correctOrientation: true,
    destinationType: this.camera.DestinationType.DATA_URL
  };
 
  // Get the data of an image
  this.camera.getPicture(options).then((imagePath) => {
    // Special handling for Android library
    if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.SAVEDPHOTOALBUM) {
this.lastImage.push(imagePath);

    

    } else {
      this.lastImage.push(imagePath);


    }
  }, (err) => {
    this.presentToast('Error while selecting image.');
  });

}


 
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}
 


// Copy the image to a local folder
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage.push(newFileName);

  }, error => {
    this.presentToast('Error while storing file.');
  });
}
 
private presentToast(text) {
  let toast = this.toastCtrl.create({
    message: text,
    duration: 3000,
    position: 'top'
  });
  toast.present();
}
 



// Always get the accurate path to your apps folder
public pathForImage(img) {


  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }


}






delImage(image){
  let index: number = this.lastImage.indexOf(image);
  if (index !== -1) {
      this.lastImage.splice(index, 1);
  }   
}





public uploadImage(id) {
  // Destination URL
  var url = "http://pinsood.com/api/v1/save_product_image/"+id+".json";
 
  // File for Upload

  for(let data of this.lastImage) {


  var targetPath = this.pathForImage(data);


console.log(targetPath); 
  alert(targetPath);
   this.presentToast(targetPath);




  // File name only
  // var filename = data;
 
  // var options = {
  //   fileKey: "file",
  //   fileName: filename,
  //   chunkedMode: false,
  //   mimeType: "multipart/form-data",
  //   params : {'fileName': filename}
  // };
 
  // const fileTransfer: TransferObject = this.transfer.create();
 
  // this.loading = this.loadingCtrl.create({
  //   content: 'Uploading...',
  // });
  // this.loading.present();
 
  // // Use the FileTransfer to upload the image

  // fileTransfer.upload(targetPath, url, options).then(data => {
  //   this.loading.dismissAll()
  //   this.presentToast('Image succesful uploaded.');
  // }, err => {
  //   this.loading.dismissAll()
  //   this.presentToast('Error while uploading file.');
  // });


}

  
}



}


