import { Component } from '@angular/core';
import { NavController,ToastController,LoadingController,NavParams ,ViewController} from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared';
import { SubcategoriesPage } from '../pages';
import {ProductsPage} from '../pages';
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html'
})


export class CategoriesPage {

  categories: any=[];
  loading: any;
  constructor(public viewCtrl: ViewController,public navCtrl: NavController,private navParams: NavParams,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {

  }
 ionViewDidEnter(){

    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      this.agornaApi.getCategories().then(data => {
        this.categories = data;


         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
        });

    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }


    categoryClicked($event,item){
        
        this.navCtrl.push(SubcategoriesPage,item);

    }



    goToProducts($event,item){
      this.navCtrl.push(ProductsPage,item)
    }

}


