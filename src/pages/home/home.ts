
import { Component } from '@angular/core';
import { MenuController,NavController,ToastController,ViewController,LoadingController,NavParams  } from 'ionic-angular';
import {ListmessagesPage,NearbyPage,SearchPage,CategoriesPage,SubcategoriesPage,HelpPage,LoginPage,SignupPage,ShopDetailPage,SettingPage,ProductsPage,ProductPage,ProductDetailPage} from '../pages'
import { Storage } from '@ionic/storage';
import { AgornaApi } from '../../shared/shared'
import { Http, Response } from '@angular/http';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { BackgroundMode } from '@ionic-native/background-mode';
import JQuery from "jquery";

declare var cordova;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',

})
export class HomePage {

  loading: any;
   products: any;
  expositions: any;
  categories: any;
  slider: any=[];
  random_expositions:any;
      i:any;
    resize:any;
sliderOption = {
        initialSlide: 0,
        loop: true,
        autoplay:5000
      };

  constructor(public menu: MenuController,public viewCtrl:ViewController,private backgroundMode: BackgroundMode , public storage: Storage,public localNotifications: LocalNotifications,public navCtrl: NavController,private navParams: NavParams,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
 this.navCtrl = navCtrl;





      var i, resize;



  // JQuery("#").click(function() {
  //   alert('sassa');
    
  // });





  }



// hmbBtn(){
// return JQuery(".div").toggleClass("cross");
// }



ionViewWillEnter() {
this.menu.enable(true);

}



ionViewWillLeave() {
this.menu.enable(true);
}

  ionViewDidLoad(){
  



      this.storage.get('intro-done').then(done => {
    if (!done) {
      this.storage.set('intro-done', true);

this.navCtrl.push(HelpPage);
    }
  });
        
    // this.agornaApi.getCategories().then(data => this.categories = data);
         
     
      this.showLoader();
  this.agornaApi.getProducts('last_products').then(data => {
      this.products = data;
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();


      });
      
            this.agornaApi.getCategories().then(data => {
        this.categories = data;

      }, (err) => {
      });
      
  this.agornaApi.getSlider().then(data => {
        this.slider = data;

      }, (err) => {
      });



      this.agornaApi.getExpositions('top_rate').then(data => {
        this.expositions = data['expositions'];
console.log(data);
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 

      });
      


            this.agornaApi.getExpositions('random_expositions').then(data => {
        this.random_expositions = data['expositions'];
console.log(data);
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 

      });
      








this.loading.dismiss();
    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
          cssClass: 'full_width_alert'
        });
 
        this.loading.present();
 
    }

goToSearch(){
  this.navCtrl.push(SearchPage, {}, {animate: true, direction: 'forward'});
}


goToProduct($event,item){
      this.navCtrl.push(ProductDetailPage,item);    
}
goToShop($event,item){
      this.navCtrl.push(ShopDetailPage,item);    
}


    categoryClicked($event,item){
        
        this.navCtrl.push(SubcategoriesPage,item);

    }

    goToCategories(){
           this.navCtrl.push(CategoriesPage);
    }
goToNearby(){
  this.navCtrl.push(NearbyPage);
}

  
 
   GoToSetting(){
    this.navCtrl.push(SettingPage);     

  }
     GoToProducts(){
    this.navCtrl.push(ProductsPage);     

  }
   

GoToLogin(){
 this.navCtrl.push(LoginPage);   
}

GoToSignup(){
 this.navCtrl.push(SignupPage);   
}

}
