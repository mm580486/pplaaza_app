import { Component } from '@angular/core';

import { AboutPage,HomePage,ContactPage,CategoriesPage } from '../pages';


@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = CategoriesPage;


  constructor() {

  }
}
