import { isDigit } from '@angular/compiler/src/chars';
import { Component, ViewChild } from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { SubcategoriesPage } from '../pages';
import { Storage } from '@ionic/storage';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MobileValidator } from  '../../validators/mobile';
import { AgeValidator } from  '../../validators/age';
import { UsernameValidator } from  '../../validators/username';

@Component({
  selector: 'page-signupExposition',
  templateUrl: 'signupExposition.html'
})


export class SignupExpositionPage {

    @ViewChild('signupSlider') signupSlider: any;
    sendAgainCodeTime: number=60;
    sendAgainCode:boolean=false;
    slideOneForm: FormGroup;
    slideTwoForm: FormGroup;
    slideThreeForm: FormGroup;
    slideFourForm: FormGroup;
    loading:any;
    submitAttempt: boolean = false;
    user:any;
    ex: any;
    username_taken:boolean;false;
    categories: any;
    i:any;
    resize:any;
  constructor(public alertCtrl: AlertController,public formBuilder: FormBuilder,public viewCtrl: ViewController,public storage: Storage,public navCtrl: NavController,private navParams: NavParams,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
    this.storage.get('user').then((value) => {
    this.user=value;
  });





  
  this.slideOneForm = formBuilder.group({  // [\u0600-\u06FF ]
        name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[ابپتثجچحیخدذرزسشطظعغفقکگلمنوهيئضص ]*'), Validators.required])],
        phone: ['', MobileValidator.isValid]
    });
 

    this.slideTwoForm = formBuilder.group({
        verifyCode: ['', Validators.compose([Validators.maxLength(7),Validators.minLength(4),Validators.required, Validators.pattern('^[0-9]*$')])]
    });

     
    this.slideThreeForm = formBuilder.group({
        identity: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9]*')])],
        category_id: ['', Validators.required],
        address: ['', Validators.required],
        detail: [''],
        exposition_name: ['', Validators.required]
    });

}






ionViewDidEnter(){

  this.signupSlider.lockSwipes(true);
}



  ionViewDidLoad(){
    this.signupSlider.lockSwipes(true);

this.agornaApi.getCategories().then(data => {
        this.categories = data;
      }, (err) => {
        });

  }




  level2(){
    this.showLoader();
      this.agornaApi.getRegisterExpositionLevel2(this.slideOneForm.value.phone,this.slideOneForm.value.name).then(data => {
         this.ex=data['ex'];
         console.log(this.ex)
         this.loading.dismiss();
         this.next();

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
        });

  }


  level3(){ 
    this.showLoader();
      this.agornaApi.getVerifyCode(this.slideTwoForm.value.verifyCode,this.ex.authentication_token).then(data => {
        if(data['status']==400){
 this.toastCtrl.create({
          message: 'کد معتبر نمیباشد',
          duration: 3000,
          position: 'bottom'
        }).present();
// this.sendAgainCode=true;
// var interval = setInterval(()=>{
// this.sendAgainCodeTime= this.sendAgainCodeTime - 1;
//  if (this.sendAgainCodeTime == 1) {
//      this.sendAgainCode=false;
//             clearInterval(interval); // If exceeded 100, clear interval
//             this.sendAgainCodeTime = 60;
//         }

// },1000);
     



        }else{
        this.next();
        }
        
         this.loading.dismiss();
       
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
        });


  }


sac(){
    this.showLoader();
      this.agornaApi.getRegisterExpositionLevel2(this.slideOneForm.value.phone,this.slideOneForm.value.name).then(data => {
         this.ex=data['ex'];
         console.log(this.ex)
         this.loading.dismiss();

this.sendAgainCode=true;
var interval = setInterval(()=>{
this.sendAgainCodeTime= this.sendAgainCodeTime - 1;
 if (this.sendAgainCodeTime == 1) {
     this.sendAgainCode=false;
            clearInterval(interval); // If exceeded 100, clear interval
            this.sendAgainCodeTime = 60;
        }

},1000);


      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
        });

}

username(v){
    if(this.slideThreeForm.controls.identity.valid){
this.agornaApi.getCheckUsername(v).then(data => {
         if(data['has']==true){
     this.username_taken=true;
         }else{
this.username_taken=false;
         }

      }, (err) => {


      })

    }else{
        this.username_taken=false;
    }

}

  verifyCode(){
     
  }

   next(){
    this.signupSlider.lockSwipes(false);
        this.signupSlider.slideNext();
        this.signupSlider.lockSwipes(true);
    }
 
    prev(){
      this.signupSlider.lockSwipes(false);
        this.signupSlider.slidePrev();
        this.signupSlider.lockSwipes(true);
    }
 



     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }


save(){
    this.submitAttempt = true;
    if(!this.slideOneForm.valid){
        this.signupSlider.slideTo(0);
    } 
    else if(!this.slideTwoForm.valid){
        this.signupSlider.slideTo(1);
    }
    else {









 this.showLoader();
      this.agornaApi.saveExposition(Object.assign(this.slideThreeForm.value,this.ex)).then(data => {
        if(data['status']==400){
 this.toastCtrl.create({
          message: 'اشکال در ثبت نام',
          duration: 3000,
          position: 'bottom'
        }).present();

        console.log(data['message'])

        }else{
 this.toastCtrl.create({
          message: 'شما با موفقیت ثبت نام کردید',
          duration: 3000,
          position: 'bottom'
        }).present();

        }
        
         this.loading.dismiss();
       
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
        });










    }
 
}






//       logForm() {


//       this.showLoader();
//       this.agornaApi.updateProfile(this.user.authentication_token,this.form_user).then(data => {
// this.storage.set('user', data);
// this.form_user=data;
// console.log(data)
//  this.toastCtrl.create({
//           message: 'پروفایل بروز شد',
//           duration: 3000,
//           position: 'bottom'
//         }).present();

//          this.loading.dismiss();
//       }, (err) => {
//  this.toastCtrl.create({
//           message: 'اشکال در برقراری ارتباط با سرور',
//           duration: 3000,
//           position: 'bottom'
//         }).present();



//  this.loading.dismiss();
//         });



  // }



 
  showRadio() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Lightsaber color');

    alert.addInput({
      type: 'radio',
      label: 'Blue',
      value: 'blue',
      checked: true
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        // this.testRadioOpen = false;
        // this.testRadioResult = data;
      }
    });
    alert.present();
  }


}


