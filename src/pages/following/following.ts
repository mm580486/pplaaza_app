import { Component } from '@angular/core';
import {  NavController,ToastController,LoadingController,NavParams,ViewController  } from 'ionic-angular';

import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { Storage } from '@ionic/storage';
import {ShopDetailPage} from '../pages'
@Component({
  selector: 'page-following',
  templateUrl: 'following.html'
})


export class FollowingPage {

  users: any;
  loading: any;
  user: any;
  constructor(public viewCtrl: ViewController, public storage: Storage,private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {


  this.storage.get('user').then((value) => {
    this.user=value;
  });

  }

ionViewDidEnter(){
     this.viewCtrl.showBackButton(false);

    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      console.log(this.navParams.data);
      this.agornaApi.getFollowing(this.user.authentication_token).then(data => {
        this.users = data['expositions'];
        console.log(data)
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });

    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }





toggleFollow($event,item,pos){


      this.agornaApi.getFollow(item.id,this.user.authentication_token,pos).then(data => {

        console.log(data)

item.followed = !pos;

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
      });


}

goToExposition($event,item){
      this.navCtrl.push(ShopDetailPage,item);    
}

}


