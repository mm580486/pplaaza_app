import { Component } from '@angular/core';
import {  Platform,PopoverController,NavController,ToastController,ViewController,LoadingController,NavParams  } from 'ionic-angular';
import { AgornaApi } from '../../shared/shared';
import { Ionic2RatingModule } from 'ionic2-rating';
import { Storage } from '@ionic/storage';
import { MenuController } from 'ionic-angular';
import { ConversationPage,StartmessagePage,FollowersPage,ProductDetailPage } from '../pages';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PopoverComponent } from '../../components/popover/popover';
import { Geolocation } from '@ionic-native/geolocation';
declare var window;
@Component({
  selector: 'page-shopDetail',
  templateUrl: 'shopDetail.html',
  
})


export class ShopDetailPage {
  filter:any='products';

  loading: any;
  exposition: any;
  exposition_data: any;
  comments:any=[];
  products:any=[];
  filters: any;
  user:any;
  content: string;
  product_data: any;
  saveBTNDisable: boolean=true;

  product_empty: boolean=false;

  filter_form: any={};

  constructor(public platform: Platform,private geolocation: Geolocation,public popoverCtrl: PopoverController,private socialSharing: SocialSharing,public menu:MenuController,public viewCtrl: ViewController,private navParams: NavParams, public storage: Storage,public navCtrl: NavController,private agornaApi: AgornaApi, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
this.exposition_data = this.navParams.data;

  this.storage.get('user').then((value) => {
    this.user=value;
  });
  
  }

  ionViewDidEnter(){

      this.storage.get('user').then((value) => {
    this.user=value;
  });

    
    console.log(this.exposition_data)
    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      this.agornaApi.getExposition(this.exposition_data.id,this.user.authentication_token).then(data => {
        this.exposition = data;
        this.exposition_data=data;
        console.log(data);
        console.log(this.exposition_data);

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();




        });

      this.agornaApi.getExpositionFilters(this.exposition_data.id).then(data => {
        this.filters = data;
        console.log('filters',data);


      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();




        });




      console.log(this.navParams.data);
      this.agornaApi.getProducts('exposition_products','',this.exposition_data.id,this.user.authentication_token).then(data => {
        this.products = data;


// setTimeout(function(){

// if(this.products.length == 0){
//   this.product_empty = true;
// }

// },500)






      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
        });





      this.agornaApi.getExpositionComments(this.exposition_data.id).then(data => {
        this.comments = data;
        console.log(data)

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
      });
      






         this.loading.dismiss();



    


  }



  shareShop() {
    
    
          
             this.platform.ready()
             .then(() =>
             {
       
                this.socialSharing.share(this.exposition.exposition_name, '', '', `https://pinsood.com/exposition/show/${this.exposition.identity}`)
                .then((data) =>
                {
                   console.log('Shared via SharePicker');
                })
                .catch((err) =>
                {
                   console.log('Was not shared via SharePicker');
                });
       
             });
    
    
    
        }
    





  startExternalMap() {

      this.platform.ready().then(() => {
        this.geolocation.getCurrentPosition().then((position) => {
          // ios
          if (this.platform.is('ios')) {
            window.open('maps://?q=' + this.exposition.exposition_address + '&saddr=' + position.coords.latitude + ',' + position.coords.longitude + '&daddr=' + this.exposition.latitude + ',' + this.exposition.longitude, '_system');
          };
          // android
          if (this.platform.is('android')) {
            window.open('geo://' + position.coords.latitude + ',' + position.coords.longitude + '?q=' + this.exposition.latitude + ',' + this.exposition.longitude + '(' + this.exposition.exposition_address + ')', '_system');
          };
        });
      });
  }

  
  goToConversation(){
    this.navCtrl.push(ConversationPage,{id: this.exposition_data.id})
  }
  
  presentPopover(myEvent,item) {
    
    
    
        let popover = this.popoverCtrl.create(PopoverComponent,{
      cb: function(_data) {
alert(_data);
  
          }
     , type: 'self'
        
    
        });
        popover.present({
          ev: myEvent
        });
    
      }


 



  goToFollowers(exposition){
this.navCtrl.push(FollowersPage,exposition);

  }

  goToProduct(item){
    this.navCtrl.push(ProductDetailPage,item);
  }

  ionViewWillLeave() {
this.menu.enable(false);
}


  toggleFavorite($event,item,pos){

    



      this.agornaApi.getFavorite(this.user.authentication_token,item.id,pos).then(data => {

        console.log(data)

        item.favorited=!pos;

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
      });




  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }

    onModelChange($event){
      alert('nazar shoma sabt shod')
    }

    callTo(number){
  number=encodeURIComponent(number);
  window.location = 'tel:'+number;
}


saveComment(){
 

    this.product_data = this.navParams.data;
    console.log(this.product_data)
    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      this.agornaApi.saveComment('nil', this.user.authentication_token ,this.content,this.exposition_data.id).then(data => {
 this.toastCtrl.create({
          message: 'نظر شما ثبت شد',
          duration: 3000,
          position: 'bottom'
        }).present();


        
         this.loading.dismiss();

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });




}


onChange($event){

  if(this.content.length > 200 || this.content.length <= 1){
this.saveBTNDisable=true;

  }else{
    this.saveBTNDisable=false;
  }

}



follow(pos){





      this.agornaApi.getFollow(this.exposition_data.id,this.user.authentication_token,pos).then(data => {

        console.log(data)

this.exposition.followed = !pos;

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
      });



}


goToTickets($event){
  this.navCtrl.push(StartmessagePage,this.exposition_data);
}

filteProducts(){
console.log(this.filter_form);
this.showLoader();
      this.agornaApi.getFilter(this.exposition_data.id,this.filter_form).then(data => {

this.products=data['products'];

if(this.products.length == 0){
  this.product_empty = true;
}




this.loading.dismiss();


      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
      });



}



}


