import { Component } from '@angular/core';
import {  NavController,ToastController,LoadingController,NavParams,ViewController  } from 'ionic-angular';

import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'

import {ProductDetailPage} from '../pages'
@Component({
  selector: 'page-products',
  templateUrl: 'products.html'
})


export class ProductsPage {

  products: any=[];
  loading: any;
  products_empty: boolean=false;
  
  constructor(public viewCtrl: ViewController,private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {



  }
doRefresh(refresher){




 this.agornaApi.getProducts('category',this.navParams.data.id).then(data => {
        this.products = data;
        if(this.products.length == 0){
          this.products_empty=true;
        }
        console.log(data)
refresher.complete();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



refresher.complete();
        });



      



}
ionViewDidEnter(){
       this.viewCtrl.showBackButton(false);

    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      console.log(this.navParams.data);
      this.agornaApi.getProducts('category',this.navParams.data.id).then(data => {
        this.products = data;
        if(this.products.length == 0){
          this.products_empty=true;
        }
        console.log(data)
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });

    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }


goToProduct($event,item){
      this.navCtrl.push(ProductDetailPage,item);    
}

}


