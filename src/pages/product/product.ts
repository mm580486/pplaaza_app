import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { ProductDetailPage,ProductCommentPage } from '../pages';
import { ViewController } from 'ionic-angular';
@Component({
  selector: 'page-product',
  templateUrl: 'product.html'
})


export class ProductPage {

  tab1Root = ProductDetailPage;
  tab2Root = ProductCommentPage;
  tab4Root = ProductCommentPage;
  params: any;
  product: any;

  constructor(public viewCtrl: ViewController,public navCtrl: NavController,private navParams: NavParams) {

      this.params = navParams;

      this.product = this.params;


  }
  ionViewDidEnter(){
  this.viewCtrl.showBackButton(false);

  }

}


