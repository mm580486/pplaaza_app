import { Component } from '@angular/core';
import {  NavController,ToastController,LoadingController,NavParams,ViewController  } from 'ionic-angular';

import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { Storage } from '@ionic/storage';
import {ProductPage,ShopPage} from '../pages';
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html'
})


export class FavoritesPage {

  products: any;
  loading: any;
  user: any;

  constructor(public viewCtrl: ViewController, public storage: Storage,private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {


    this.storage.get('user').then((value) => {
    this.user=value;
  });

  }

ionViewDidEnter(){








    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      console.log(this.navParams.data);
      this.agornaApi.getFavorites(this.user.authentication_token).then(data => {
        this.products = data;
        console.log(data)
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });

    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }


     toggleFavorite($event,item,pos){

    



      this.agornaApi.getFavorite(this.user.authentication_token,item.id,pos).then(data => {

        console.log(data)

        item.favorited=!pos;

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
      });




  }


goToProduct($event,item){
      this.navCtrl.push(ProductPage,item);    
}


goToExposition($event,item){
  this.navCtrl.push(ShopPage,{id: item.exposition_id,exposition_name: item.exposition_name});  
}

}


