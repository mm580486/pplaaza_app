import { Component } from '@angular/core';

import {  NavController,ToastController,LoadingController,NavParams,ModalController,ViewController  } from 'ionic-angular';


import { Storage } from '@ionic/storage';
import { AgornaApi } from '../../shared/shared'
import { Http, Response } from '@angular/http';
@Component({
  selector: 'page-buildComment',
  templateUrl: 'buildComment.html'
  
})


export class BuildCommentPage {

  loading: any;
  content: string;
  product_data: any;
  saveBTNDisable: boolean=true;
  user: any;
  constructor( public storage: Storage, public viewCtrl: ViewController,private navParams: NavParams,public navCtrl: NavController,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController,private agornaApi: AgornaApi) {

  }



  
    
ionViewDidLoad(){
  this.storage.get('token').then((value) => {
    this.user=value;
  });
}


     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }


     closeModal() {
    this.viewCtrl.dismiss();
  }


saveComment(){
 

    this.product_data = this.navParams.data;
    console.log(this.product_data)
    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      this.agornaApi.saveComment(this.product_data.product_id, this.user ,this.content).then(data => {
 this.toastCtrl.create({
          message: 'نظر شما ثبت شد',
          duration: 3000,
          position: 'bottom'
        }).present();

        console.log(data);
        console.log(this.product_data);
        
         this.loading.dismiss();
         this.viewCtrl.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });




}


onChange($event){

  if(this.content.length > 20 || this.content.length <= 1){
this.saveBTNDisable=true;

  }else{
    this.saveBTNDisable=false;
  }

}


    



}


