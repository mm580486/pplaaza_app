import { Component } from '@angular/core';
import { NavController,ToastController,LoadingController,NavParams,ViewController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { SubcategoriesPage } from '../pages';
import { Storage } from '@ionic/storage';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})


export class ProfilePage {

  categories: any;
  loading: any;
  user: any;
  form_user:any={};
  constructor(public viewCtrl: ViewController,public storage: Storage,public navCtrl: NavController,private navParams: NavParams,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
    this.storage.get('user').then((value) => {
    this.user=value;
  });


 

}


  ionViewDidEnter(){
this.viewCtrl.showBackButton(false);
    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      this.agornaApi.getProfile(this.user.authentication_token).then(data => {
        this.form_user = data;
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });

    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }




      logForm() {


      this.showLoader();
      this.agornaApi.updateProfile(this.user.authentication_token,this.form_user).then(data => {
this.storage.set('user', data);
this.form_user=data;
console.log(data)
 this.toastCtrl.create({
          message: 'پروفایل بروز شد',
          duration: 3000,
          position: 'bottom'
        }).present();

         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });



  }



 

}


