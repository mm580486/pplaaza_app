import { Component } from '@angular/core';
import { NavController,ToastController,LoadingController,NavParams } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import {HomePage} from '../pages'
import { Storage } from '@ionic/storage';
@Component({
  selector: 'page-help',
  templateUrl: 'help.html'
})


export class HelpPage {
  cat: any;
  categories: any;
  loading: any;
  constructor(private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {

  }


    goToHome($event,item){
      this.navCtrl.setRoot(HomePage)
    }

}


