import { Component } from '@angular/core';
import { NavController,NavParams,ViewController } from 'ionic-angular';
import { ShopDetailPage,ProductsPage } from '../pages';
@Component({
  selector: 'page-shop',
  templateUrl: 'shop.html'
})


export class ShopPage {
  tab1Root = ShopDetailPage;

  tab2Root = ProductsPage;
  // tab3Root = ContactPage;
  public params: any;
  public exposition: any;


  constructor(public viewCtrl: ViewController,public navCtrl: NavController,private navParams: NavParams) {
      this.params = navParams;
      this.exposition = this.params;


  }

  ionViewDidEnter(){
     
this.viewCtrl.showBackButton(false);

  }

}


