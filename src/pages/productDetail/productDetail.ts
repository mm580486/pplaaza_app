import { Component } from '@angular/core';
import { PopoverController, NavController,ToastController,LoadingController,NavParams,ViewController,AlertController  } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { ShopDetailPage,ImageViewPage,ConversationPage } from '../pages';
import { Storage } from '@ionic/storage';
import {Platform} from 'ionic-angular';
import { PopoverComponent } from '../../components/popover/popover';


@Component({
  selector: 'page-productDetail',
  templateUrl: 'productDetail.html'
})


export class ProductDetailPage {
  loading: any;
  product: any;
  product_data: any;
  without_images:boolean=false;
  content: string;
  saveBTNDisable: boolean=true;
  user:any;
  products: any;
  filter: string='product_detail';
  comments: any;
  screen_width: number=500;
  constructor(public platform   : Platform,private socialSharing: SocialSharing,public popoverCtrl: PopoverController,private alertCtrl: AlertController,public storage: Storage,public viewCtrl:ViewController, private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {


       



    this.storage.get('user').then((value) => {
    this.user=value;
  });
  }



  presentPopover(myEvent,item) {
    
    
    
        let popover = this.popoverCtrl.create(PopoverComponent,{
      cb: function(_data) {
alert(_data);
  
          }
     , type: 'self'
        
    
        });
        popover.present({
          ev: myEvent
        });
    
      }





    shareProduct() {


      
         this.platform.ready()
         .then(() =>
         {
   
            this.socialSharing.share(this.product.name, '', this.product.images[0], `https://pinsood.com/product/${this.product.id}`)
            .then((data) =>
            {
               console.log('Shared via SharePicker');
            })
            .catch((err) =>
            {
               console.log('Was not shared via SharePicker');
            });
   
         });



    }


    saveComment(){
 

    this.product_data = this.navParams.data;
    console.log(this.product_data)
    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      this.agornaApi.saveComment(this.product_data.id, this.user.authentication_token ,this.content,'').then(data => {
 this.toastCtrl.create({
          message: 'نظر شما ثبت شد',
          duration: 3000,
          position: 'bottom'
        }).present();
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 this.loading.dismiss();
        });

}

  
onChange($event){

  if(this.content.length > 200 || this.content.length <= 1){
this.saveBTNDisable=true;

  }else{
    this.saveBTNDisable=false;
  }

}




goToConversation(){
  this.navCtrl.push(ConversationPage,{id: this.product.exposition_id})
}
favorited(pos){



 this.agornaApi.getFavorite(this.user.authentication_token, this.product_data.id,pos).then(data => {

        console.log(data)

        this.product.favorited=!pos;

      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();
 
      });



}



getMessage(){
  this.presentPrompt();
}

presentPrompt() {
  let alert = this.alertCtrl.create({
    title: 'پیام',
    inputs: [
      {
        name: 'message',
        placeholder: 'پیام',
        type: 'text'
      }
    ],
    buttons: [
      {
        text: 'انصراف',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'ارسال',
        handler: data => {
          if(data.message == ''){
return false
          }else{
            this.navCtrl.push(ConversationPage,{id: this.product.exposition_id,default_message: '$' + this.product.id + ' ' +data.message ,product_id: this.product_data.id});
          }
         
        }
      }
    ]
  });
  alert.present();
}





presentPromptOffer() {
  let alert = this.alertCtrl.create({
    title: 'قیمت پیشنهادی',
    inputs: [
      {
        name: 'message',
        placeholder: 'قیمت',
        type: 'number'
      }
    ],
    buttons: [
      {
        text: 'انصراف',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'ارسال',
        handler: data => {
          if(data.message == ''){
return false
          }else{
            this.navCtrl.push(ConversationPage,{id: this.product.exposition_id,default_message: '$' + this.product.id + ' سلام من محصول شما را به  ' +data.message + 'تومان میخواهم ، ایا امکانش هست ؟',product_id: this.product_data.id});
          }
         
        }
      }
    ]
  });
  alert.present();
}


  ionViewDidEnter(){
    this.viewCtrl.showBackButton(false);

    this.product_data = this.navParams.data;
    console.log(this.product_data)
    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();

      this.agornaApi.getProducts('last_products').then(data => {
        this.products = data;

        }, (err) => {
   this.toastCtrl.create({
            message: 'اشکال در برقراری ارتباط با سرور',
            duration: 3000,
            position: 'bottom'
          }).present();
  
  
        });
      this.agornaApi.getProduct(this.product_data.id,this.user.authentication_token).then(data => {
        this.product = data;
        this.product_data = data;
        if(this.product.images.length == 0){
this.without_images=true;
        }


        console.log(data);
        console.log(this.product_data);
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();




        });

    



    



 this.agornaApi.getComments(this.product_data.id).then(data => {
        this.comments = data;
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();




        });

        this.loading.dismissAll();

  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }
    goToProduct(item){
      this.navCtrl.push(ProductDetailPage,item)
    }




goToExposition(){

this.navCtrl.push(ShopDetailPage,{id: this.product_data.exposition_id,exposition_name: this.product_data.exposition_name});

  
}

goToImageView(item){
// this.photoViewer.show(item);
this.navCtrl.push(ImageViewPage,{ srcs: item })
}





}