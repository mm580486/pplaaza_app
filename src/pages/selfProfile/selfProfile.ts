import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AgornaApi } from '../../shared/shared';
import { Storage } from '@ionic/storage';
import { ProductDetailPage,ShopDetailPage,ProfilePage } from '../pages';

declare var window;
@Component({
  selector: 'page-selfProfile',
  templateUrl: 'selfProfile.html'
})
export class SelfProfilePage {
products: any;
users: any;
user: any;
filter: any = 'product';
  constructor(public storage: Storage,public navCtrl: NavController,private agornaApi: AgornaApi) {
    this.storage.get('user').then((value) => {
      this.user=value;
    });

  }


ionViewDidEnter(){
  this.agornaApi.getFavorites(this.user.authentication_token).then(data => {
    this.products = data;


  }, (err) => {

    });


    this.agornaApi.getFollowing(this.user.authentication_token).then(data => {
      this.users = data['expositions'];
    }, (err) => {
      });

}


goToProduct(item){
  this.navCtrl.push(ProductDetailPage,item);
}

goToShop(item){
  this.navCtrl.push(ShopDetailPage,item);
}

goToEditPage(){
  this.navCtrl.push(ProfilePage);
}




toggleFollow($event,item,pos){
  
  
        this.agornaApi.getFollow(item.id,this.user.authentication_token,pos).then(data => {
  
          console.log(data)
  
  item.followed = !pos;
  
        }, (err) => {

   
        });
  
  
  }



  toggleFavorite($event,item,pos){
    
        
    
    
    
          this.agornaApi.getFavorite(this.user.authentication_token,item.id,pos).then(data => {
            item.favorited=!pos;
          }, (err) => {

     
          });
    
    
    
    
      }

}
