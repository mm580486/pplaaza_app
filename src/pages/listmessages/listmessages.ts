import { Component } from '@angular/core';
import {  NavController,ToastController,LoadingController,NavParams  } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { Storage } from '@ionic/storage';
import { ConversationPage } from '../pages';
import { ViewController } from 'ionic-angular';
// import { Ng2Cable } from 'ng2-cable/js/index';
@Component({
  selector: 'page-listmessages',
  templateUrl: 'listmessages.html'
})


export class ListmessagesPage {

loading: any;
tickets: any;
token: any;
tickets_empty:boolean=false;
  constructor( public viewCtrl: ViewController,public storage: Storage,private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {

    this.storage.get('token').then((value) => {
    this.token=value;
  });

  }

  ionViewDidEnter(){
     this.viewCtrl.showBackButton(false);


      this.showLoader();
      console.log(this.navParams.data);
      this.agornaApi.getTickets(this.token).then(data => {
        this.tickets = data;
if(this.tickets.length == 0){
  this.tickets_empty=true;
}else{
  this.tickets_empty=false;
}

        console.log(data)
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });

    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }


    goToConversation($event,item){
      this.navCtrl.push(ConversationPage,item);
    }




}


