import { Component } from '@angular/core';
import { NavController,ToastController,LoadingController,NavParams,ViewController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import {ProductsPage} from '../pages';

@Component({
  selector: 'page-subcategories',
  templateUrl: 'subcategories.html'
})


export class SubcategoriesPage {
  cat: any;
  categories: any;
  loading: any;
  constructor(public viewCtrl: ViewController,private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {

  }
  ionViewDidEnter(){

     
this.viewCtrl.showBackButton(false);
    this.cat = this.navParams.data;
    
    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      this.agornaApi.getSubCategories(this.cat.id).then(data => {
        this.categories = data;
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });

    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }

    goToProducts($event,item){
      this.navCtrl.push(ProductsPage,item)
    }

}


