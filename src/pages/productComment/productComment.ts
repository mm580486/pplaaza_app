import { Component } from '@angular/core';
import {  NavController,ToastController,LoadingController,NavParams,ModalController,ViewController  } from 'ionic-angular';

import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared';
import {BuildCommentPage} from '../pages';

@Component({
  selector: 'page-productComment',
  templateUrl: 'productComment.html'
})


export class ProductCommentPage {

  loading: any;
  comments: any;
 public product_data: any;
  constructor(public viewCtrl: ViewController,public modalCtrl: ModalController,private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {

  }



  ionViewDidEnter(){
      this.viewCtrl.showBackButton(false);
    this.product_data = this.navParams.data;
    console.log(this.product_data)
    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      this.agornaApi.getComments(this.product_data.id).then(data => {
        this.comments = data;
        console.log(data);
        console.log(this.product_data);
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });

    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }




presentProfileModal() {
  //  let profileModal = this.modalCtrl.create();
  this.navCtrl.push(BuildCommentPage, { product_id: this.product_data.id });
  
 }



    



}


