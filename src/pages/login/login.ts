import { Component } from '@angular/core';
import { NavController, LoadingController,ToastController,ViewController } from 'ionic-angular';
import { Auth } from '../../providers/auth/auth';
import {SignupPage,HomePage} from '../pages'
import {App} from 'ionic-angular';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
 
    email: string;
    password: string;
    loading: any;

    constructor(public viewCtrl: ViewController,public toastCtrl: ToastController,public app: App,public navCtrl: NavController,public authService: Auth, public loadingCtrl: LoadingController) {
 
    }
 
    ionViewDidLoad() {
 this.viewCtrl.showBackButton(false);

 
    }
 
    login(){
 
        this.showLoader();
 
        let credentials = {
            email: this.email,
            password: this.password
        };
 
        this.authService.login(credentials).then((result) => {
            this.loading.dismiss();
            console.log(result);

            

   let toast = this.toastCtrl.create({
          message: 'شما با موفقیت وارد شدید',
          duration: 3000,
          position: 'bottom'
        });

        toast.present();
          setTimeout(function() {
 
window.location.reload(true);
  }, 2000);


        
        }, (err) => {
            this.loading.dismiss();

   let toast = this.toastCtrl.create({
          message: 'نام کاربری یا رمز عبور اشتباه است',
          duration: 3000,
          position: 'bottom'
        });

        toast.present();

            console.log(err);
        });
 
    }
 
    launchSignup(){
        this.navCtrl.push(SignupPage);
    }

    goToHome(){
        this.navCtrl.setRoot(HomePage);
    }
 
    showLoader(){
 
        this.loading = this.loadingCtrl.create({
            
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }
 
}