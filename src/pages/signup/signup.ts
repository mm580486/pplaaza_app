import { isDigit } from '@angular/compiler/src/chars';
import { Component, ViewChild } from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Auth } from '../../providers/auth/auth';
import { HomePage } from '../home/home';
import { SignupExpositionPage } from '../pages'
 import { Toast } from '@ionic-native/toast';
 import { FormBuilder, FormGroup, Validators } from '@angular/forms';
 import { MobileValidator } from  '../../validators/mobile';
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  @ViewChild('signupSlider') signupSlider: any;
  name: string;
  phone: string;
  email: string;
  password: string;
  loading:any;
  signup_group: FormGroup;
  submitAttempt: boolean = false;
  slideOneForm: FormGroup;
  slideTwoForm: FormGroup;
  slideThreeForm: FormGroup;
  slideFourForm: FormGroup;
  
  constructor(public viewCtrl: ViewController, public formBuilder: FormBuilder,public navCtrl: NavController, public authService: Auth, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
 

   this.signup_group = formBuilder.group({
        name: ['',  Validators.compose([Validators.maxLength(30), Validators.pattern('[ابپتثجچحیخدذرزسشطظعغفقکگلمنوهيئضص ]*'), Validators.required])],
        email: ['',Validators.compose([Validators.required, Validators.pattern('/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/')])],
        phone: ['', MobileValidator.isValid],
        password: ['', Validators.required],
        date: ['', Validators.required],
        gender: ['', Validators.required],
        family: ['', Validators.required]
    });





  }


  level2(){

    

         this.next();



  }

  next(){
    this.signupSlider.lockSwipes(false);
    this.signupSlider.slideNext();
    this.signupSlider.lockSwipes(true);
}

prev(){
  this.signupSlider.lockSwipes(false);
    this.signupSlider.slidePrev();
    this.signupSlider.lockSwipes(true);
}

   
    ionViewDidLoad() {
      this.signupSlider.onlyExternal = true;
     this.signupSlider.lockSwipes(true);
 
    }
  goToHome(){
        this.navCtrl.setRoot(HomePage);
    }

    goToSignupExposition(){
      this.navCtrl.push(SignupExpositionPage);
    }
 
  register(){
 

this.submitAttempt = true;




    this.showLoader();
 
    let details = {
        name: this.name,
        email: this.email,
        password: this.password,
        phone: this.phone
    };

    console.log(this.signup_group.value);

    this.authService.createAccount(this.signup_group.value).then((result) => {
      this.loading.dismiss();

        let toast = this.toastCtrl.create({
          message: 'شما با موفقیت ثبت نام کردید',
          duration: 3000,
          position: 'bottom'
        });

        toast.present();
        
      console.log(result);
      this.navCtrl.setRoot(HomePage);

    }, (err) => {

        let toast = this.toastCtrl.create({
          message: 'اشکال در ثبت کاربر ، فرم را با دفت پر کنید',
          duration: 3000,
          position: 'bottom'
        });

        toast.present();
        
      
        this.loading.dismiss();
    });
 
  }
 
  showLoader(){
 
    this.loading = this.loadingCtrl.create({
      content: 'درحال پردازش...'
    });
 
    this.loading.present();
 
  }

  goToRoot(){
    this.navCtrl.setRoot(HomePage);
  }
 
}