import { Component } from '@angular/core';
import {  NavController,ToastController,LoadingController,NavParams,ViewController  } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { AgornaApi } from '../../shared/shared'
import { Storage } from '@ionic/storage';
import {ProductDetailPage} from '../pages';
import { PopoverComponent } from '../../components/popover/popover';
@Component({
  selector: 'page-selfProduct',
  templateUrl: 'selfProduct.html'
})


export class SelfProductPage {

  products: any;
  loading: any;
  token: string;
  constructor(public viewCtrl: ViewController,public popoverCtrl: PopoverController,public storage: Storage,private navParams: NavParams,public navCtrl: NavController,private agornaApi: AgornaApi,private http: Http, public loadingCtrl: LoadingController,private toastCtrl: ToastController) {

    this.storage.get('token').then((value) => {
    this.token=value;
  });

}




  presentPopover(myEvent,item) {



    let popover = this.popoverCtrl.create(PopoverComponent,{
  cb: function(_data) {
if(_data=='delete'){


      this.showLoader();
      this.agornaApi.deleteProduct(this.token,item.id).then(data => {
        this.products = data;
        console.log(data)
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });



}





      }
 , type: 'self'
    

    });
    popover.present({
      ev: myEvent
    });

  }


trash($event,item){

      this.agornaApi.deleteProduct(this.token,item.id).then(data => {
        this.products = data['products'];
        console.log(data)
 this.toastCtrl.create({
          message: 'محصول با موفقیت حذف شد',
          duration: 3000,
          position: 'bottom'
        }).present();


      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();




        });



}




ionViewDidEnter(){
     
this.viewCtrl.showBackButton(false);
    // this.agornaApi.getCategories().then(data => this.categories = data);
      this.showLoader();
      console.log(this.navParams.data);
      this.agornaApi.getProducts('self',this.token).then(data => {
        this.products = data;
        console.log(data)
         this.loading.dismiss();
      }, (err) => {
 this.toastCtrl.create({
          message: 'اشکال در برقراری ارتباط با سرور',
          duration: 3000,
          position: 'bottom'
        }).present();



 this.loading.dismiss();
        });

    


  }

     showLoader(){
 
        this.loading = this.loadingCtrl.create({
            content: 'در حال پردازش...'
        });
 
        this.loading.present();
 
    }


goToProduct($event,item){
      this.navCtrl.push(ProductDetailPage,item);    
}

}


