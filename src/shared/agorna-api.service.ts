import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Injectable } from '@angular/core';
import { Http, Response,Request } from '@angular/http';
import 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as io from 'socket.io-client';
import * as Base64 from 'crypto-js/enc-base64';
@Injectable()
export class AgornaApi {
  socketObserver: any; 
  socketService: any;
  socket: any;
    private baseUrl = 'http://pplaaza.com/api/v1';
    socketHost: string = 'http://localhost:3000';
    constructor(private http: Http){ 
 this.socketService = Observable.create(observer => {
        this.socketObserver = observer;
      });

    }


    getSubCategories(id){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/subcategories/${id}.json`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }


    getCategories(){
        return new Promise((resolve, reject) => {
        this.http.get(`${this.baseUrl}/categories.json`)
            .subscribe(res => resolve(res.json()), (err) => {
        reject(err);
      });

    });   
}

        getProducts(type,category='nil',exposition_id='nil',token='nil'){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/products/${type}.json?category_id=${category}&exposition_id=${exposition_id}&token=${token}`)
                .subscribe(res => resolve(res.json().products), (err) => {
            reject(err);
          });

        });   
    }

            getFavorites(token){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/favorites/${token}.json`)
                .subscribe(res => resolve(res.json().products), (err) => {
            reject(err);
          });

        });   
    }

        getFollowing(token){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/following/${token}.json`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }

    




                getFavorite(token,product_id,pos){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/favorite/${token}.json?product_id=${product_id}&favorited=${pos}`)
                .subscribe(res => resolve(res), (err) => {
            reject(err);
          });

        });   
    }



            getComments(product_id){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/product_comments/${product_id}.json`)
                .subscribe(res => resolve(res.json().comments), (err) => {
            reject(err);
          });

        });   
    }

                getStartMessage(token,exposition_id,title,body){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/startmessage/${token}.json?id=${exposition_id}&title=${title}&body=${body}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }



                getExpositionComments(exposition_id){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/exposition_comments/${exposition_id}.json`)
                .subscribe(res => resolve(res.json().comments), (err) => {
            reject(err);
          });

        });   
    }



        getProduct(id,token){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/product/${id}.json?token=${token}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }

 getExposition(id,token='nil'){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/exposition/${id}.json?token=${token}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }

 getFollow(id,token='nil',pos){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/follow/${id}.json?token=${token}&followed=${pos}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }


     getTickets(token){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/tickets/${token}.json`)
                .subscribe(res => resolve(res.json().tickets), (err) => {
            reject(err);
          });

        });   
    }

         hasNewMessage(token){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/hasNewTickets/${token}.json`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }


   getConversation(token,id,message=''){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/conversation/${token}.json?id=${id}&message=${message}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }
   getExpositionFilters(id){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/exposition_filters/${id}.json`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }


       getRegisterExpositionLevel2(phone,name){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/register_exposition.json?phone=${phone}&name=${name}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }

    getVerifyCode(code,token){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/verify/${token}.json?code=${code}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }



    getCheckUsername(identity){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/check_username/${identity}.json`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }



       getFilter(id,filter_form){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/filter/${id}.json?filter_form=${JSON.stringify(filter_form)}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }


   saveConversation(token,id,message){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/build_conversation/${token}.json?id=${id}&message=${message}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }
       checkNewMessage(token,id,last_message_id){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/checkNewMessage/${token}.json?id=${id}&last_message_id=${last_message_id}`)
                .subscribe(res => resolve(res.json().conversations), (err) => {
            reject(err);
          });

        });   
    }

           getProfile(token){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/profile/${token}.json`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }

 updateProfile(token,form_user){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/update_profile/${token}.json?form_user=${JSON.stringify(form_user)}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }

     saveProduct(token,form_product){
         let postParams={
form_product: JSON.stringify(form_product)
         }
            return new Promise((resolve, reject) => {
            this.http.post(`${this.baseUrl}/save_product/${token}.json`,postParams)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }


         saveExposition(form_user){
         let postParams={
form_user: JSON.stringify(form_user)
         }
            return new Promise((resolve, reject) => {
            this.http.post(`${this.baseUrl}/register_exposition_c.json`,postParams)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }



         getNearby(latitude,longitude){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/nearby.json?latitude=${latitude}&longitude=${longitude}`)
                .subscribe(res => resolve(res.json().expositions), (err) => {
            reject(err);
          });

        });   
    }





deleteProduct(token,product_id){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/delete_product/${token}.json?product_id=${product_id}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }

search(filter,q,token=''){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/search/${filter}.json?q=${q}&token=${token}`)
                .subscribe(res => resolve(res.json() ), (err) => {
            reject(err);
          });

        });   
    }


rate(token,exposition_id,rate){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/rate/${token}.json?exposition_id=${exposition_id}&rate=${rate}`)
                .subscribe(res => resolve(res.json() ), (err) => {
            reject(err);
          });

        });   
    }



 getProductCategories(token){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/product_categories/${token}.json`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }

     getCategoryFields(token,category_id){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/category_fields/${token}.json?category_id=${category_id}`)
                .subscribe(res => resolve(res.json().fields), (err) => {
            reject(err);
          });

        });   
    }


     getSlider(){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/slider.json`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }

    



        getExpositions(type){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/expositions/${type}.json`)
                .subscribe(res => resolve(res.json()), (err) => {
                    
            reject(err);
          });

        });   
    }

        saveComment(id='',token,content,ex_id=''){
            return new Promise((resolve, reject) => {
            this.http.get(`${this.baseUrl}/save_comment/${id}.json?token=${token}&content=${content}&exposition_id=${ex_id}`)
                .subscribe(res => resolve(res.json()), (err) => {
            reject(err);
          });

        });   
    }


}