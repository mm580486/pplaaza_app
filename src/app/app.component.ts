import { SelfProductPage } from '../pages/selfProduct/selfProduct';
import { SignupPageModule } from '../pages/signup/signup.module';
import { HomePage } from '../pages/home/home';
import { Component,Inject, ViewChild } from '@angular/core';
import { Platform,App, NavController ,ToastController,AlertController ,Alert} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import {HTTP_PROVIDERS} from '@angular/http';
// import { TabsPage } from '../pages/tabs/tabs';
import { FavoritesPage } from '../pages/favorites/favorites';
import { CategoriesPage } from '../pages/categories/categories';
import { BuildProductPage } from '../pages/buildProduct/buildProduct';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { ListmessagesPage } from '../pages/listmessages/listmessages';
import { FollowingPage } from '../pages/following/following';
import { SignupPage } from '../pages/signup/signup';
import { NearbyPage } from '../pages/nearby/nearby';
import { AboutPage } from '../pages/about/about';
import { ShopDetailPage } from '../pages/shopDetail/shopDetail';
import { SelfProfilePage } from '../pages/selfProfile/selfProfile';
import { SettingPage } from '../pages/setting/setting';
import { ProductDetailPage } from '../pages/productDetail/productDetail';
import { HelpPage } from '../pages/help/help';
import { AgornaApi } from '../shared/shared';
import { Auth } from '../providers/auth/auth';
import { Storage } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { BackgroundMode } from '@ionic-native/background-mode';
import {Deeplinks} from '@ionic-native/deeplinks';
import { Http, Response,Request } from '@angular/http';
import 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as io from 'socket.io-client';
import $ from 'jquery';
import { Network } from '@ionic-native/network';


declare var cordova;

declare var navigator: any;
declare var Connection: any;
@Component({
  templateUrl: 'app.html',
  providers:[
    Network,
LocalNotifications
  ] 

})

export class MyApp {
  @ViewChild('content') nav
  rootPage:any = HomePage;
  public user: any;
  public nothin:any=[];
  public isExposition:boolean=false;



  constructor(private deeplinks: Deeplinks,public alertCtrl: AlertController ,public toastCtrl: ToastController, private network: Network,public http: Http,public agornaApi:AgornaApi ,private backgroundMode: BackgroundMode ,public localNotifications: LocalNotifications,public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public storage: Storage,public authService: Auth) {

    this.storage.get('user').then((value) => {
    this.user=value;
  })
  
  





// document.addEventListener('deviceready', function () {  



// cordova.plugins.notification.local.on("click", function (notification) {
// this.nav.push(ListmessagesPage);
// });
//     // Android customization
//     // To indicate that the app is executing tasks in background and being paused would disrupt the user.
//     // The plug-in has to create a notification while in background - like a download progress bar.
//     cordova.plugins.backgroundMode.setDefaults({ 
//         title:  'بارگزاری',
//         text:   ''
//     });
//     // Enable background mode
//     cordova.plugins.backgroundMode.enable();

//     // Called when background mode has been activated
//     cordova.plugins.backgroundMode.onactivate = function () {

//         // Set an interval of 30 minutes (1800000 milliseconds)



//           setInterval(function () {

//  // The code that you want to run repeatedly
//     // cordova.plugins.notification.local.schedule({

//     //     text: 'aasaas',
//     //     data: { secret:'key' }
//     // });



// // cordova.plugins.backgroundMode.setDefaults({
// //     title: 'پیام',
// //     text: 'پیام جدید' // this will look for icon.png in platforms/android/res/drawable|mipmap

// // })

//   // this.http.get("").map(res => res).subscribe(data => {


// if(window.localStorage.getItem('token') != 'null'){


//     let path="https://www.pinsood.com/api/v1/hasNewTickets/"+window.localStorage.getItem('token')+".json";
//     $.get(path, function( data ) {
//       console.log(data);
//   if(data['size'] != 0){
//     cordova.plugins.notification.local.schedule({
//             text: `شما ${data['size']}پیام از  ${data['from']} نفر دارید `,
//             data: { secret:'key' }
//         });
//   }
// });


// }

// // this.agornaApi.hasNewTickets('EnYYGEbNAbw7Hrhtgk5o').then(data => {

 

//     // });

 
    
// // this.nothin=data;



//     // for(let item of this.nothin){






//       //  cordova.plugins.notification.local.schedule({
//       //   text: 'proccess error',
//       //   data: { secret:'key' }
//     // });


//     // cordova.plugins.notification.local.schedule({
//     //     text: err,
//     //     sound:  'file://beep.caf',
//     //     data: { secret:'key' }
//     // });

//         }, 10000);
//     }
// }, false);


// ***********************************************


  
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.


      this.deeplinks.routeWithNavController(this.nav, {
        '/about-us': AboutPage,
        '/product/:id': ProductDetailPage,
        '/exposition/show/:id': ShopDetailPage
      }).subscribe((match) => {
        // match.$route - the route we matched, which is the matched entry from the arguments to route()
        // match.$args - the args passed in the link
        // match.$link - the full link data

      }, (nomatch) => {
        // nomatch.$link - the full link data

      });
    


      statusBar.styleDefault();


    

          setTimeout(() => {

      // stuff if disconnected
      console.log(this.network.type);
 if (this.network.type == 'none' ) { 
        // stuff if disconnected
    cordova.plugins.notification.local.schedule({
            text: `برای استفاده از نرم افزار به اینترنت متصل شوید `,
            data: { secret:'key' }
        });


      } else {
        //stuff if connected
// this.alert.create({
//                 title: "Connection Status",
//                 subTitle: 'دستگاه به اینترنت متصل نیست',
//                 buttons: ["OK"]
//             }).present();

        splashScreen.hide();
      }
 
     
      }, 100);




// setTimeout(() => {
//        cordova.plugins.diagnostic.isRemoteNotificationsEnabled(function(enabled){
//     console.log("Remote notifications are " + (enabled ? "enabled" : "disabled"));
// }, function(error){
//     console.error("The following error occurred: "+error);
// });

// cordova.plugins.diagnostic.isRegisteredForRemoteNotifications(function(registered){
//     console.log("Device " + (registered ? "is" : "isn't") + " registered for remote notifications");
// }, function(error){
//     console.error("The following error occurred: "+error);
// });

// cordova.plugins.diagnostic.isRemindersAuthorized(function(authorized){
//     console.log("App is " + (authorized ? "authorized" : "denied") + " access to reminders");
// }, function(error){
//     console.error("The following error occurred: "+error);
// });
//  }, 7000);








        


    });
  }


 




  
hmbBtn(){
  if(document.getElementsByTagName("ion-menu")[0].classList.contains('show-menu')==true){
    return document.getElementsByClassName("div")[0].classList.add("cross");
  }else{
    return document.getElementsByClassName("div")[0].classList.remove("cross");
  }
}

 mmm(){

   
if(window.localStorage.getItem('token') != 'null'){
    let path="https://www.pinsood.com/api/v1/hasNewTickets/"+window.localStorage.getItem('token')+".json";
    $.get(path, function( data ) {
  if(data['size'] != 0){
alert('man')
  }
});


}
 }



logout(){
    this.storage.set('token', '');
    this.storage.set('user', '');
    var storage = window.localStorage;
    storage.removeItem('token');
    setTimeout(function() {
      window.location.reload(true);
    }, 1000);

}

goToSignUp(){
  this.nav.push(SignupPage);
}
goToAbout(){
  this.nav.push(AboutPage);
}
goToLogin(){
  this.nav.push(LoginPage);
}
goToHome(){
  this.nav.setRoot(HomePage);
}
goToSelfProduct(){
   this.nav.push(SelfProductPage);
}

goToSelf(){
  this.nav.push(SelfProfilePage);
}
 goToHelp(){
    this.nav.setRoot(HelpPage);
 }

 goToFollowing(){
   this.nav.push(FollowingPage);
   
 }

goToListmessages(){
  this.nav.push(ListmessagesPage);
}

goToBuildProduct(){
    this.nav.push(BuildProductPage);
}

goToNearby(){
  this.nav.push(NearbyPage)
}

goToSetting(){
  this.nav.push(SettingPage);
}

 goToFavorites(){
// this.rootPage=FavoritesPage;
this.nav.push(FavoritesPage);
  }

   goToCategories(){
// this.rootPage=FavoritesPage;
this.nav.push(CategoriesPage);
  }

  goToProfile(){
    this.nav.push(ProfilePage);
  }


  
   

}

