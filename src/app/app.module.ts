import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SelfProfilePage,FollowersPage,SignupExpositionPage,ImageViewPage,NearbyPage,StartmessagePage,FollowingPage,SearchPage, SelfProductPage,BuildProductPage,HelpPage,ProfilePage,ConversationPage,BuildCommentPage,FavoritesPage,ListmessagesPage,SubcategoriesPage,SignupPage,LoginPage,ProductCommentPage,ProductDetailPage,ProductPage,CategoriesPage,ProductsPage,SettingPage,AboutPage,ContactPage,HomePage,TabsPage,ShopPage,ShopDetailPage } from '../pages/pages';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Auth } from '../providers/auth/auth';
import { IonicStorageModule } from '@ionic/storage';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Geolocation } from '@ionic-native/geolocation';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import {Deeplinks} from '@ionic-native/deeplinks';
import jQuery from "jquery";
import { AgornaApi } from '../shared/shared';
import { Network } from '@ionic-native/network';
import { Ionic2RatingModule } from 'ionic2-rating';
import { PopoverComponent } from '../components/popover/popover';
import { ProductComponent } from '../components/product/product';
import { ExpositionComponent } from '../components/exposition/exposition';
import { EmojiPickerModule } from '@ionic-tools/emoji-picker';





@NgModule({
  declarations: [

    MyApp,
    AboutPage,
    ProductComponent,
    ExpositionComponent,
    ContactPage,
    HomePage,
    TabsPage,
    ShopPage,
    ShopDetailPage,
    SettingPage,
    ProductsPage,
    SelfProfilePage,
    CategoriesPage,
    ProductPage,
    ProductDetailPage,
    ProductCommentPage,
    SignupPage,
    LoginPage,
    SubcategoriesPage,
    ListmessagesPage,
    FavoritesPage,
    BuildCommentPage,
    ConversationPage,
    ProfilePage,
    HelpPage,
    BuildProductPage,
    SelfProductPage,
    PopoverComponent,
    SearchPage,
    FollowingPage,
    StartmessagePage,
    NearbyPage,
    ImageViewPage,
    SignupExpositionPage,
    FollowersPage
  ],
  imports: [
    BrowserModule,
    HttpModule,  
    Ionic2RatingModule,
    EmojiPickerModule.forRoot(),    
    IonicModule.forRoot(MyApp, {}, {
      links: [
       { component: AboutPage, name: 'About', segment: 'about' }
     ]
   }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProductComponent,
    ExpositionComponent,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ShopPage,
    ShopDetailPage,
    SettingPage,
    ProductsPage,
    SelfProfilePage,
    HelpPage,
    CategoriesPage,
    ProductPage,
    ProductDetailPage,
    ProductCommentPage, 
    SignupPage,
    LoginPage,
    SubcategoriesPage,
    ListmessagesPage,
    FavoritesPage,
    BuildCommentPage,
    ConversationPage,
    ProfilePage,
    BuildProductPage,
    SelfProductPage,
    PopoverComponent,
    SearchPage,
    FollowingPage,
    StartmessagePage,
    NearbyPage,
    ImageViewPage,
    SignupExpositionPage,
    FollowersPage
  ],
  providers: [
    StatusBar,File, Camera, Transfer,AgornaApi, 
    FilePath,
    SplashScreen,Deeplinks,
    SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Auth,Network,
    Geolocation,
    BackgroundMode
  ]
})
export class AppModule {}
